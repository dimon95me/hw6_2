package dimon.java.HomeWork4.model;

/**
 * Created by Dimon on 06.04.18.
 */
public class YearBook extends Sheet {
    private String subjects;
    private String publishist;

    public YearBook(String name, String subjects, String publishing, int year) {
        super(name, year);
        this.subjects = subjects;
        this.publishist = publishing;
    }

    public void print() {
        System.out.println("-----------");
        System.out.println(super.name);
        System.out.println(subjects);
        System.out.println(publishist);
        System.out.println(super.year);
    }
}
