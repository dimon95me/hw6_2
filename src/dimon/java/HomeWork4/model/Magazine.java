package dimon.java.HomeWork4.model;

import java.util.Date;

/**
 * Created by Dimon on 06.04.18.
 */
public class Magazine extends Sheet {
    private String subjects;
    private int month;

    public Magazine(String name, String subjects, int month, int year) {
        super(name, year);
        this.subjects = subjects;
        this.month = month;
    }

    public void print() {
        System.out.println("-----------");
        System.out.println(super.name);
        System.out.println(subjects);
        if (month < 10) {
            System.out.println(String.format("0%s.%s", month, year));
        } else {
            System.out.println(String.format("%s.%s", month, year));
        }
    }
}
