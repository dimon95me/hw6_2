package dimon.java.HomeWork4.model;

/**
 * Created by Dimon on 06.04.18.
 */
public class Book extends Sheet {
    private String author;
    private String publishist;

    public Book(String name, String author, String publishist, int year) {
        super(name, year);
        this.author = author;
        this.publishist = publishist;
    }

    public void print() {
        System.out.println("-----------");
        System.out.println(super.name);
        System.out.println(publishist);
        System.out.println(super.year);
    }
}
