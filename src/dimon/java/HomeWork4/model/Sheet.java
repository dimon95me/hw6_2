package dimon.java.HomeWork4.model;

/**
 * Created by Dimon on 08.04.18.
 */
public class Sheet {
    protected String name;
    protected int year;

    public Sheet(String name, int year) {
        this.name = name;
        this.year = year;
    }

    public boolean verifyYear(int year) {
        return this.year < year;
    }

    public void print() {
        System.out.println("-----------");
        System.out.println(name);
        System.out.println(year);
    }
}
