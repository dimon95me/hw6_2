package dimon.java.HomeWork4;

import dimon.java.HomeWork4.model.Sheet;

/**
 * Created by Dimon on 08.04.18.
 */
public class Library {
    private Sheet[] sheets;

    public Library(Sheet[] sheets) {
        this.sheets = sheets;
    }

    public void printIfYear(int year) {
        for (Sheet sheet : sheets) {
            if (sheet.verifyYear(year)) {
                sheet.print();
            }
        }
    }

    public void printIfYear(String string) {
        int year = Integer.parseInt(string);
        for (Sheet sheet : sheets) {
            if (sheet.verifyYear(year)) {
                sheet.print();
            }
        }
    }

    public void printAll() {
        for (Sheet sheet : sheets) {
            sheet.print();
        }
    }
}
