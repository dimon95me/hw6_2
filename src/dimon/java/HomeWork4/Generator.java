package dimon.java.HomeWork4;

import dimon.java.HomeWork4.model.Book;
import dimon.java.HomeWork4.model.Magazine;
import dimon.java.HomeWork4.model.Sheet;
import dimon.java.HomeWork4.model.YearBook;

/**
 * Created by Dimon on 06.04.18.
 */
public class Generator {
    public Generator() {
    }

    public static Sheet[] generateSheet() {
        Sheet[] sheets = new Sheet[9];

        String[] bookNames = {"Maugli", "Gaydamaki", "Robinson Cruzo"};
        String[] bookAuthors = {"Rediard Kipling", "Taras Shevchenko", "Daniel Defo"};
        String[] bookPublishers = {"Ababahalamaga", "Ranok", "Osvita"};
        int[] bookYears = {1942, 1841, 1719};

        sheets[0] = new Book(bookNames[0], bookAuthors[0], bookPublishers[0], bookYears[0]);
        sheets[1] = new Book(bookNames[1], bookAuthors[1], bookPublishers[1], bookYears[1]);
        sheets[2] = new Book(bookNames[2], bookAuthors[2], bookPublishers[2], bookYears[2]);

        String[] magazineNames = {"New York Times", "BBC Journal", "Men's health"};
        String[] magazineSubjects = {"Worldest economy", "Cougars", "Health food"};
        int[][] magazineDates = {{5, 2005}, {4, 2008}, {9, 2015}};

        sheets[3] = new Magazine(magazineNames[0], magazineSubjects[0], magazineDates[0][0], magazineDates[0][1]);
        sheets[4] = new Magazine(magazineNames[1], magazineSubjects[1], magazineDates[1][0], magazineDates[1][1]);
        sheets[5] = new Magazine(magazineNames[2], magazineSubjects[2], magazineDates[2][0], magazineDates[2][1]);

        String[] yearBookNames = {"Football", "Demography", "Business"};
        String[] yearBookSubjets = {"Sport", "Cosial", "Economy"};
        String[] yearBookPublishings = {"Souz Pechat", "National Typography", "Times"};
        int[] yearBookYears = {1867, 2005, 2018};

        sheets[6] = new YearBook(yearBookNames[0], yearBookSubjets[0], yearBookPublishings[0], yearBookYears[0]);
        sheets[7] = new YearBook(yearBookNames[1], yearBookSubjets[1], yearBookPublishings[1], yearBookYears[1]);
        sheets[8] = new YearBook(yearBookNames[2], yearBookSubjets[2], yearBookPublishings[2], yearBookYears[2]);

        return sheets;
    }
}
